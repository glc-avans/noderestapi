#! /bin/bash
export N_PREFIX="$HOME/n"; [[ :$PATH: == *":$N_PREFIX/bin:"* ]] || PATH+=":$N_PREFIX/bin"
#/home/pi/n/bin/node ~/noderestapi/api/lib/index.js
NODE=~/n/bin/node
NPM=~/n/bin/npm
chromium-browser --display=:0 --app=http://localhost --start-fullscreen &
cd ~/noderestapi/api/ && npm run start 

