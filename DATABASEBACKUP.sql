CREATE DATABASE  IF NOT EXISTS `gilze` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `gilze`;
-- MySQL dump 10.13  Distrib 5.7.17, for macos10.12 (x86_64)
--
-- Host: 192.168.1.195    Database: gilze
-- ------------------------------------------------------
-- Server version	5.5.54-0+deb7u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `flight`
--

DROP TABLE IF EXISTS `flight`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flight` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `FLIGHTDAY_ID` int(11) DEFAULT NULL,
  `PLANE_ID` int(11) NOT NULL,
  `DATETIME_START` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `DATETIME_END` timestamp NULL DEFAULT NULL,
  `TYPE` varchar(32) NOT NULL DEFAULT 'regular',
  `COMMENT` varchar(256) CHARACTER SET ascii DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FLIGHT_PLAN LINK` (`FLIGHTDAY_ID`),
  KEY `PLANE LINK` (`PLANE_ID`),
  CONSTRAINT `FLIGHT_PLAN LINK` FOREIGN KEY (`FLIGHTDAY_ID`) REFERENCES `flight_day` (`ID`) ON UPDATE NO ACTION,
  CONSTRAINT `PLANE LINK` FOREIGN KEY (`PLANE_ID`) REFERENCES `plane` (`ID`) ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flight`
--

LOCK TABLES `flight` WRITE;
/*!40000 ALTER TABLE `flight` DISABLE KEYS */;
INSERT INTO `flight` VALUES (1,1,1,'2017-05-11 09:06:35','2017-05-11 09:06:55','regular',NULL),(2,1,1,'2017-05-11 09:27:24','2017-05-11 09:32:13','regular',NULL),(3,1,1,'2017-05-11 09:32:27','2017-05-11 09:32:57','regular',NULL),(4,1,1,'2017-05-11 09:46:35','2017-05-11 09:52:47','regular',NULL),(5,1,1,'2017-05-11 09:52:49','2017-05-11 09:55:07','regular',NULL),(6,1,1,'2017-05-11 09:56:40','2017-05-11 09:57:30','regular',NULL),(7,1,1,'2017-05-11 09:58:30','2017-05-11 09:59:12','regular',NULL),(8,1,1,'2017-05-11 09:59:17','2017-05-11 10:04:00','regular',NULL),(9,1,1,'2017-05-11 10:04:02','2017-05-11 10:04:13','regular',NULL),(10,1,1,'2017-05-11 10:04:36','2017-05-11 10:04:52','regular',NULL),(11,1,1,'2017-05-11 10:05:15','2017-05-11 10:09:31','regular',NULL),(12,1,1,'2017-05-11 10:09:33','2017-05-11 10:09:56','regular',NULL),(13,1,1,'2017-05-11 10:12:42','2017-05-11 10:12:55','regular',NULL),(14,1,1,'2017-05-11 10:17:14','2017-05-11 10:17:24','regular',NULL),(15,1,1,'2017-05-11 10:17:27','2017-05-11 10:17:44','regular',NULL),(16,1,1,'2017-05-11 10:17:48','2017-05-11 10:18:32','regular',NULL),(17,1,1,'2017-05-11 10:24:55','2017-05-11 10:26:13','regular',NULL),(18,1,1,'2017-05-11 10:26:19','2017-05-11 10:35:28','regular',NULL),(19,1,1,'2017-05-11 10:32:42','2017-05-11 10:33:12','regular',NULL),(20,1,1,'2017-05-11 10:33:21','2017-05-11 10:33:31','regular',NULL),(21,1,1,'2017-05-11 10:33:57','2017-05-11 10:34:17','regular',NULL),(22,1,1,'2017-05-11 10:34:24','2017-05-11 10:34:37','regular',NULL),(23,1,1,'2017-05-11 10:35:06','2017-05-11 10:42:09','regular',NULL),(24,1,1,'2017-05-11 10:40:36','2017-05-11 10:40:50','regular',NULL),(25,1,1,'2017-05-11 10:41:27','2017-05-11 10:41:39','regular',NULL),(26,1,1,'2017-05-11 10:41:50','2017-05-11 10:44:23','regular',NULL),(27,1,1,'2017-05-11 10:44:44','2017-05-11 10:45:11','regular',NULL),(28,1,1,'2017-05-11 10:45:17','2017-05-11 10:52:20','regular',NULL),(29,1,1,'2017-05-11 10:52:36','2017-05-11 10:53:46','regular',NULL),(30,1,1,'2017-05-11 10:53:57','2017-05-11 10:54:37','regular',NULL),(31,1,1,'2017-05-11 10:55:23','2017-05-11 10:55:48','regular',NULL),(32,1,1,'2017-05-11 10:56:02','2017-05-11 10:56:23','regular',NULL),(33,1,1,'2017-05-11 10:56:30','2017-05-11 10:56:57','regular',NULL),(34,1,1,'2017-05-11 11:05:16','2017-05-11 11:09:40','regular',NULL),(35,1,1,'2017-05-11 11:10:11',NULL,'regular',NULL);
/*!40000 ALTER TABLE `flight` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flight_day`
--

DROP TABLE IF EXISTS `flight_day`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flight_day` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flight_day`
--

LOCK TABLES `flight_day` WRITE;
/*!40000 ALTER TABLE `flight_day` DISABLE KEYS */;
INSERT INTO `flight_day` VALUES (1,'2017-05-01 13:04:39');
/*!40000 ALTER TABLE `flight_day` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `has_flight_user`
--

DROP TABLE IF EXISTS `has_flight_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `has_flight_user` (
  `USER_ID` int(11) NOT NULL,
  `FLIGHT_ID` int(11) NOT NULL,
  PRIMARY KEY (`FLIGHT_ID`,`USER_ID`),
  KEY `user_fk` (`USER_ID`),
  CONSTRAINT `flight_fk` FOREIGN KEY (`FLIGHT_ID`) REFERENCES `flight` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `user_fk` FOREIGN KEY (`USER_ID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `has_flight_user`
--

LOCK TABLES `has_flight_user` WRITE;
/*!40000 ALTER TABLE `has_flight_user` DISABLE KEYS */;
INSERT INTO `has_flight_user` VALUES (1,27),(1,28),(1,29),(1,30),(1,31),(1,33),(2,27),(2,28),(2,29),(2,30),(2,31),(2,32),(2,33),(2,34),(2,35);
/*!40000 ALTER TABLE `has_flight_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `plane`
--

DROP TABLE IF EXISTS `plane`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `plane` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `REGISTRATION` varchar(10) CHARACTER SET ascii NOT NULL,
  `NAME` varchar(64) CHARACTER SET ascii NOT NULL,
  `MODEL` varchar(64) CHARACTER SET ascii NOT NULL,
  `COMMENT` varchar(256) CHARACTER SET ascii NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `REGISTRATION` (`REGISTRATION`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `plane`
--

LOCK TABLES `plane` WRITE;
/*!40000 ALTER TABLE `plane` DISABLE KEYS */;
INSERT INTO `plane` VALUES (1,'reg','name','model','comment');
/*!40000 ALTER TABLE `plane` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reader`
--

DROP TABLE IF EXISTS `reader`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reader` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `READER_ID` varchar(64) CHARACTER SET ascii NOT NULL,
  `PLANE_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `READER_ID` (`READER_ID`),
  KEY `plane_fk` (`PLANE_ID`),
  CONSTRAINT `plane_fk` FOREIGN KEY (`PLANE_ID`) REFERENCES `plane` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reader`
--

LOCK TABLES `reader` WRITE;
/*!40000 ALTER TABLE `reader` DISABLE KEYS */;
INSERT INTO `reader` VALUES (1,'ff34',1),(2,'f232',1);
/*!40000 ALTER TABLE `reader` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(256) CHARACTER SET ascii NOT NULL,
  `CARD_ID` varchar(64) CHARACTER SET ascii NOT NULL,
  `ROLE` varchar(64) CHARACTER SET ascii NOT NULL DEFAULT 'Pilot' COMMENT 'TODO: ENUM',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `CARD_ID` (`CARD_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'Diederich Kroeske','6aa12200','Pilot'),(2,'Kenneth van Ewijk','221e2600','Student');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'gilze'
--

--
-- Dumping routines for database 'gilze'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-05-18 11:04:50
