CREATE DATABASE  IF NOT EXISTS `gilze` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `gilze`;
-- MySQL dump 10.13  Distrib 5.7.17, for macos10.12 (x86_64)
--
-- Host: 192.168.1.195    Database: gilze
-- ------------------------------------------------------
-- Server version	5.5.55-0+deb8u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `flight`
--

DROP TABLE IF EXISTS `flight`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flight` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `FLIGHTDAY_ID` int(11) DEFAULT NULL,
  `PLANE_ID` int(11) NOT NULL,
  `DATETIME_START` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `DATETIME_END` timestamp NULL DEFAULT NULL,
  `TYPE` varchar(32) NOT NULL DEFAULT 'regular',
  `COMMENT` varchar(256) CHARACTER SET ascii DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FLIGHT_PLAN LINK` (`FLIGHTDAY_ID`),
  KEY `PLANE LINK` (`PLANE_ID`),
  CONSTRAINT `FLIGHT_PLAN LINK` FOREIGN KEY (`FLIGHTDAY_ID`) REFERENCES `flight_day` (`ID`) ON UPDATE NO ACTION,
  CONSTRAINT `PLANE LINK` FOREIGN KEY (`PLANE_ID`) REFERENCES `plane` (`ID`) ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flight`
--

LOCK TABLES `flight` WRITE;
/*!40000 ALTER TABLE `flight` DISABLE KEYS */;
/*!40000 ALTER TABLE `flight` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flight_day`
--

DROP TABLE IF EXISTS `flight_day`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flight_day` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flight_day`
--

LOCK TABLES `flight_day` WRITE;
/*!40000 ALTER TABLE `flight_day` DISABLE KEYS */;
INSERT INTO `flight_day` VALUES (1,'2017-05-01 13:04:39');
/*!40000 ALTER TABLE `flight_day` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `has_flight_user`
--

DROP TABLE IF EXISTS `has_flight_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `has_flight_user` (
  `USER_ID` int(11) NOT NULL,
  `FLIGHT_ID` int(11) NOT NULL,
  PRIMARY KEY (`FLIGHT_ID`,`USER_ID`),
  KEY `user_fk` (`USER_ID`),
  CONSTRAINT `flight_fk` FOREIGN KEY (`FLIGHT_ID`) REFERENCES `flight` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `user_fk` FOREIGN KEY (`USER_ID`) REFERENCES `user` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `has_flight_user`
--

LOCK TABLES `has_flight_user` WRITE;
/*!40000 ALTER TABLE `has_flight_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `has_flight_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `plane`
--

DROP TABLE IF EXISTS `plane`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `plane` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `REGISTRATION` varchar(10) CHARACTER SET ascii NOT NULL,
  `NAME` varchar(64) CHARACTER SET ascii NOT NULL,
  `MODEL` varchar(64) CHARACTER SET ascii NOT NULL,
  `COMMENT` varchar(256) CHARACTER SET ascii NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `REGISTRATION` (`REGISTRATION`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `plane`
--

LOCK TABLES `plane` WRITE;
/*!40000 ALTER TABLE `plane` DISABLE KEYS */;
INSERT INTO `plane` VALUES (1,'PH-1099','G3','G3','Dit is een geweldig vliegtuig'),(2,'PH-1337','G6','G6','Dit is een geweldiger vliegtuig'),(3,'H46','G9','Model 6','YEAH BOIIII55'),(4,'R-07','G007','Model 3','Not stirred');
/*!40000 ALTER TABLE `plane` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reader`
--

DROP TABLE IF EXISTS `reader`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reader` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `READER_ID` varchar(64) CHARACTER SET ascii NOT NULL,
  `PLANE_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `READER_ID` (`READER_ID`),
  KEY `plane_fk` (`PLANE_ID`),
  CONSTRAINT `plane_fk` FOREIGN KEY (`PLANE_ID`) REFERENCES `plane` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reader`
--

LOCK TABLES `reader` WRITE;
/*!40000 ALTER TABLE `reader` DISABLE KEYS */;
/*!40000 ALTER TABLE `reader` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(256) CHARACTER SET ascii NOT NULL,
  `CARD_ID` varchar(64) CHARACTER SET ascii NOT NULL,
  `ROLE` varchar(64) CHARACTER SET ascii NOT NULL DEFAULT 'Pilot' COMMENT 'TODO: ENUM',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `CARD_ID` (`CARD_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'gilze'
--

--
-- Dumping routines for database 'gilze'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-06-23 12:13:28
