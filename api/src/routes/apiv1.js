import express      from 'express';
import jwt          from 'jwt-simple';
import httpCodes    from 'http-status-codes';
import moment       from 'moment';

import db from '../util/database.js';

const router = express.Router();

const ANONYMOUS = 'Anonymous';
const ANONYMOUS_PLANE_ID = null;

router.get('/', (req, res) => {
    var response = [
        '/readers/',
        '/flights/',
        '/users/',
        '/planes/',
    ];
    res.status(200);
    res.json(formatResponse(true, httpCodes.OK, 'Here are the URLS', response));
});

const dbDefaultError = (res, error) => {
    console.log('ERROR: ' + error);
    res.status(httpCodes.INTERNAL_SERVER_ERROR);
    res.json(formatResponse(false, httpCodes.INTERNAL_SERVER_ERROR, 'Something went wrong during the database lookup.', error));
    throw error;
};

const formatResponse = (success, code, message, data = null) => {
    const response = {
        status_code: code,
        status_message: message,
        success: success,
        data: data,
    };
    return response;
};

const defaultGetQuery = (res, query, getType) => {
    db.execute(query)
    .then((response) => {
        res.status(httpCodes.OK);
        res.json(formatResponse(true, httpCodes.OK, 'Succesfully got ' + getType + '(s)', response.results));
    })
    .catch((error) => {
        dbDefaultError(res, error);
    });
};

router.get('/wipe/', (req, res) => {
    const query = db.format('SET FOREIGN_KEY_CHECKS = 0; ' +
        'TRUNCATE gilze.has_flight_user;' +
        'TRUNCATE gilze.flight;' +
        'TRUNCATE gilze.user;' +
        'TRUNCATE gilze.reader;' +
        'SET FOREIGN_KEY_CHECKS = 1;');
    db.execute(query)
    .then((response) => {
        res.status(httpCodes.OK);
        res.json(formatResponse(true, httpCodes.OK, 'Succesfully wiped database flight info'));
    })
    .catch((error) => {
        dbDefaultError(res, error);
    });
});

router.get('/planes/:id?', (req, res) => {
    const planeId = req.params.id || null;
    var query = db.format('SELECT * FROM plane;');

    if (planeId) {
        query = db.format('SELECT * FROM plane WHERE plane.ID = ?', [planeId]);
    }

    defaultGetQuery(res, query, 'plane');
});

router.post('/planes/', (req, res) => {
    const {registration, name, model, comment} = req.body;

    const query = db.format('INSERT INTO plane (REGISTRATION, NAME, MODEL, COMMENT) VALUES(?, ?, ?, ?);', [registration, name, model, comment]);

    db.execute(query)
    .then((response) => {
        res.status(httpCodes.OK);
        res.json(formatResponse(true, httpCodes.OK, 'Updated plane succesfully'));
    })
    .catch((error) => {
        dbDefaultError(res, error);
    });
});

router.put('/planes/:id', (req, res) => {
    const planeId = req.params.id || null;

    const {registration, name, model, comment} = req.body;

    if (planeId) {
        const query = db.format('UPDATE plane SET REGISTRATION = ?, NAME = ?, MODEL = ?, COMMENT = ? WHERE ID = ?;', [registration, name, model, comment, planeId]);

        db.execute(query)
        .then((response) => {
            console.log(response);
            res.status(httpCodes.OK);
            res.json(formatResponse(true, httpCodes.OK, 'Updated plane succesfully'));
        })
        .catch((error) => {
            dbDefaultError(res, error);
        });
    } else {
        res.status(httpCodes.BAD_REQUEST);
        res.json(formatResponse(false, httpCodes.BAD_REQUEST, 'Missing parameters planeId', {planeId, registration, name, model, comment}));
    }
});

router.get('/readers/anonymous/', (req, res) => {
    const query = db.format('SELECT * FROM reader WHERE PLANE_ID IS NULL;');

    defaultGetQuery(res, query, 'reader');
})

router.get('/readers/:id?', (req, res) => {
    const readerId = req.params.id || null;

    var query = db.format('SELECT reader.ID, reader.READER_ID, plane.ID as PLANE_ID, plane.REGISTRATION, plane.NAME, plane.MODEL, plane.COMMENT FROM reader LEFT JOIN plane ON plane.ID = reader.PLANE_ID;');
    if (readerId) {
        query = db.format('SELECT reader.ID, reader.READER_ID, plane.ID as PLANE_ID, plane.REGISTRATION, plane.NAME, plane.MODEL, plane.COMMENT FROM reader LEFT JOIN plane ON plane.ID = reader.PLANE_ID WHERE reader.ID = ?', [readerId]);
    }
    db.execute(query)
    .then((response) => {
        const returnValue = response.results.map((reader) => {
            if (reader['PLANE_ID'] === null) {
                reader['PLANE'] = null;
            } else {
                reader['PLANE'] = {
                    'ID': reader['PLANE_ID'],
                    'REGISTRATION': reader['REGISTRATION'],
                    'NAME': reader['NAME'],
                    'MODEL': reader['MODEL'],
                    'COMMENT': reader['COMMENT'],
                }
            }
            delete reader['PLANE_ID'];
            delete reader['REGISTRATION'];
            delete reader['NAME'];
            delete reader['MODEL'];
            delete reader['COMMENT'];
            return reader;
        });
        res.status(httpCodes.OK);
        res.json(formatResponse(true, httpCodes.OK, 'Succesfully got reader(s)', returnValue));
    })
    .catch((error) => {
        dbDefaultError(res, error);
    });
    // defaultGetQuery(res, query, 'reader');
});

router.put('/readers/:id?', (req, res) => {
    const readerId = req.params.id || null;
    const planeId = req.body.planeId || null;
    if (readerId && planeId) {
        const query = db.format('UPDATE reader SET PLANE_ID = ? WHERE ID = ?;', [planeId, readerId]);

        db.execute(query)
        .then((response) => {
            res.status(httpCodes.OK);
            res.json(formatResponse(true, httpCodes.OK, 'Succesfully updated planeId'));
        })
        .catch((error) => {
            dbDefaultError(res, error);
        });
    } else {
        res.status(httpCodes.BAD_REQUEST);
        res.json(formatResponse(false, httpCodes.BAD_REQUEST, 'Missing parameters planeId or readerId', {planeId, readerId}));
    }
});

router.get('/flights/:flightId?', (req, res) => {
    const flightId = req.params.flightId || null;
    var query = db.format('SELECT flight.*, has_flight_user.USER_ID, user.NAME, user.ROLE, user.CARD_ID, plane.REGISTRATION, plane.MODEL, plane.NAME as PLANE_NAME, plane.COMMENT, plane.ID as PLANE_ID FROM flight, has_flight_user, user, plane  WHERE flight.ID = has_flight_user.FLIGHT_ID  AND user.ID = has_flight_user.USER_ID AND flight.PLANE_ID = plane.ID ORDER BY ID DESC;');
    if (flightId) {
        query = db.format('SELECT flight.*, has_flight_user.USER_ID, user.NAME, user.ROLE, user.CARD_ID, plane.REGISTRATION, plane.MODEL, plane.NAME as PLANE_NAME, plane.COMMENT, plane.ID as PLANE_ID FROM flight, has_flight_user, user, plane  WHERE flight.ID = has_flight_user.FLIGHT_ID  AND user.ID = has_flight_user.USER_ID AND flight.PLANE_ID = plane.ID AND flight.ID = ? ORDER BY ID DESC;', [flightId]);
    }
    db.execute(query)
    .then((response) => {
        const results = response.results.reduce((allResults, curr, index) => {
                // if ID's are the same
                // combine users
                const foundResult = allResults.find((flight) => {
                    return curr['ID'] === flight['ID'];
                });
                if (foundResult) { // id already in allResults
                    // already found so we have to append the users to found result
                    foundResult['USERS'].push({
                        'NAME': curr['NAME'],
                        'ROLE': curr['ROLE'],
                        'CARD_ID': curr['CARD_ID'],
                        'ID': curr['USER_ID'],
                    });
                    return allResults;
                } else {
                    // not found yet so add to allResults
                    console.log('Curr is');
                    console.log(curr);
                    curr['PLANE'] = {
                        'ID': curr['PLANE_ID'],
                        'NAME': curr['PLANE_NAME'],
                        'MODEL': curr['MODEL'],
                        'REGISTRATION': curr['REGISTRATION'],
                        'COMMENT': curr['COMMENT'],
                    };
                    delete curr['MODEL'];
                    delete curr['REGISTRATION'];
                    delete curr['COMMENT'];
                    delete curr['PLANE_NAME'];
                    delete curr['PLANE_ID'];
                    curr['USERS'] = [];
                    curr['USERS'].push({
                        'ID': curr['USER_ID'],
                        'NAME': curr['NAME'],
                        'ROLE': curr['ROLE'],
                        'CARD_ID': curr['CARD_ID'],
                    });
                    delete curr['NAME'];
                    delete curr['ROLE'];
                    delete curr['CARD_ID'];
                    delete curr['USER_ID'];
                    return allResults.concat(curr);
                }

        }, []);
        res.status(httpCodes.OK);
        res.json(formatResponse(true, httpCodes.OK, 'Succesfully got flight(s)', results));
    })
    .catch((error) => {
        dbDefaultError(res, error);
    });
});

router.put('/flights/:flightId?/:stop?', (req, res) => {
    const flightId = req.params.flightId || null;
    const stop = req.params.stop || null;
    const body = req.body || null;

    const paramNotSpecified = (param, param2) => {
        res.status(httpCodes.BAD_REQUEST);
        res.json(formatResponse(false, httpCodes.BAD_REQUEST, 'The ' + param + ' parameter is not specified correctly.'));
    };

    var query = db.format('UPDATE flight SET DATETIME_START = ?, DATETIME_END = ? WHERE ID = ?;', [moment(body.dateTimeStart).format(), moment(body.dateTimeEnd).format(),flightId]);
    if (stop) {
        query = db.format('UPDATE flight SET DATETIME_END = CURRENT_TIMESTAMP() WHERE ID = ?;', [flightId]);
    }  else if (!body) {
        paramNotSpecified('body');
        return;
    }
    db.execute(query)
    .then((response) => {
        res.status(httpCodes.OK);
        if (stop) {
            res.json(formatResponse(true, httpCodes.OK, 'Succesfully stopped flight.'));
        } else {
            res.json(formatResponse(true, httpCodes.OK, 'Succesfully changed values of flight.'));
        }
    })
    .catch((error) => {
        dbDefaultError(res, error);
    });
});

router.post('/users/check/:option?/', (req, res) => {
    const option = req.params.option || null;
    const body = req.body || null;
    console.log(body);

    // local functions;
    const paramNotSpecified = (param, param2) => {
        res.status(httpCodes.BAD_REQUEST);
        res.json(formatResponse(false, httpCodes.BAD_REQUEST, 'The ' + param + ' parameter or ' + param2 + ' (in/out) is not specified correctly.'));
    };

    const notRegisteredInDb = (param) => {
        res.status(httpCodes.NOT_FOUND);
        res.json(formatResponse(false, 0, 'This '+ param + ' is not registered in the database yet. Please do this first.'));
    };

    if (option === 'in') {
        // check in
        if (body.cardId && body.readerId) {
            // card id specified, can check in
            const query = db.format('SELECT ID,NAME FROM user WHERE CARD_ID = ?; SELECT ID, PLANE_ID FROM reader WHERE READER_ID = ?', [body.cardId, body.readerId]);
            db.execute(query)
            .then((response) => {
                if (response.results[0].length === 0) {
                    // not found in db
                    // notRegisteredInDb('card');
                    // Insert an anonymous user so that this card can be bound to a user.
                    const query = db.format('INSERT INTO user (NAME, CARD_ID) VALUES (?, ?);', [ANONYMOUS, body.cardId]);
                    db.execute(query)
                    .then((response) => {
                        res.status(httpCodes.OK);
                        res.json(formatResponse(true, 0, 'Anonymous user added to the database.'));
                    })
                    .catch((error) => {
                        dbDefaultError(res, error);
                    });
                    return Promise.reject(); // cut to the final catch without giving any errors, so handle the error above here.
                } else if (response.results[1].length === 0) {
                    // notRegisteredInDb('reader');
                    //reader is not registered in db, so register it without a valid plane ID
                    const query = db.format('INSERT INTO reader (READER_ID, PLANE_ID) VALUES (?, ?);', [body.readerId, ANONYMOUS_PLANE_ID]);
                    db.execute(query)
                    .then((response) => {
                        console.log(response);
                        res.status(httpCodes.OK);
                        res.json(formatResponse(true, 0, 'New reader added to the database'));
                    })
                    .catch((error) => {
                        dbDefaultError(res, error);
                    })
                    return Promise.reject();
                } else {
                    // found in db
                    // check in
                    const flightDayId = 1; //TODO: remove
                    const userId = response.results[0][0]['ID']; // can be index 0 because unique constraint.
                    const planeId = response.results[1][0]['PLANE_ID'];
                    if (response.results[0][0]['NAME'] === 'Anonymous') {
                        console.log('User is anonymous, so prohibit checkin');
                        res.status(httpCodes.INTERNAL_SERVER_ERROR);
                        res.json(formatResponse(false, 0, 'An anonymous user can not checkin.'));
                        return Promise.reject();
                    }
                    console.log('HIER');
                    console.log(planeId);
                    if (!planeId) {
                        console.log('PlaneId is null, kortom de reader is niet gekoppeld, eerst koppelen!');
                        res.status(httpCodes.INTERNAL_SERVER_ERROR);
                        res.json(formatResponse(false, 0, 'The reader is not bound to a plane, please bind this first.'));
                        return Promise.reject();
                    }
                    const query = db.format('SELECT flight.ID AS FLIGHT_ID, flight.DATETIME_START, NOW() AS SERVER_TIME FROM flight, user, has_flight_user WHERE flight.ID = has_flight_user.FLIGHT_ID AND has_flight_user.USER_ID = user.ID AND flight.PLANE_ID = ? AND CARD_ID = ? AND DATETIME_END IS NULL ORDER BY DATETIME_START DESC LIMIT 0,1;', [planeId, body.cardId]);
                    return db.execute(query, {flightDayId, userId, planeId});
                }
            })
            .then((response) => {
                if (response.results.length === 0) {
                    // check in
                    console.log('checkin');
                    // query here has the option to leave DATETIME_END out of this query, so we can check if the flight has ENDED below.
                    const query1 = db.format('SELECT ID, DATETIME_START, NOW() AS SERVER_TIME from flight WHERE PLANE_ID = ? AND DATETIME_END IS NULL ORDER BY DATETIME_START DESC LIMIT 0,1;', [response.extraParams.planeId])
                    return db.execute(query1, response.extraParams);
                } else {
                    console.log('checkout');
                    // check uit als de checkin langer dan 10 sec geleden was.
                    const serverTime = moment(response.results[0]['SERVER_TIME']);
                    serverTime.subtract(10, 'seconds'); // servertime set to 10 seconds ago
                    const flightTime = moment(response.results[0]['DATETIME_START']);
                    // checkout
                    const flightId = response.results[0]['FLIGHT_ID'];

                    if (flightTime.isAfter(serverTime)) {
                        // checked in within 10 seconds, so give error instead of checkout.
                        res.status(httpCodes.INTERNAL_SERVER_ERROR);
                        res.json(formatResponse(false, 3, 'User is already checked in'));
                        return Promise.reject();
                    } else {
                        const query = db.format('UPDATE flight SET DATETIME_END = NOW() WHERE ID IN (?);', [flightId]);
                        db.execute(query)
                        .then((response) => {
                            res.status(httpCodes.OK);
                            res.json(formatResponse(true, 2, 'Checked out succesfully'));
                        })
                        .catch((error) => {
                            dbDefaultError(res, error);
                        });
                        return Promise.reject();
                    }
                }
            })
            .then((response) => {
                // response.results[0]['ID'] is ons id.
                // if null, insert (volgende query)
                // if not null, kortom binnen 5 minuten, dan insert in kopfeltabel
                var query = db.format('INSERT INTO flight (FLIGHTDAY_ID, PLANE_ID) VALUES (?, ?); INSERT INTO has_flight_user (USER_ID, FLIGHT_ID) VALUES(?, LAST_INSERT_ID());',
                    [
                        response.extraParams.flightDayId,
                        response.extraParams.planeId,
                        response.extraParams.userId
                    ]);
                if (response.results[0] && response.results[0].length !== null) {
                    const flightId = response.results[0]['ID'];
                    const serverTime = moment(response.results[0]['SERVER_TIME']);
                    const flightTime = moment(response.results[0]['DATETIME_START']);
                    serverTime.subtract(5, 'minutes'); // servertime set to 5 minutes ago
                    if (flightTime.isAfter(serverTime)) {
                        // if flightTime is after serverTime -5m, so if flightTime is within last 5 minutes.
                        // this is the same flight, we should just add this user to the kopfeltabel.
                        query = db.format('INSERT INTO has_flight_user (USER_ID, FLIGHT_ID) VALUES(?, ?);', [response.extraParams.userId, flightId]);
                    }
                }
                db.execute(query)
                .then((response) => {
                    if (response.results) {
                        // check in succesful
                        res.status(httpCodes.OK);
                        res.json(formatResponse(true, 1, 'Checked in succesfully'));
                    } else {
                        // something went wrong during check in
                        res.status(httpCodes.INTERNAL_SERVER_ERROR);
                        res.json(formatResponse(false, httpCodes.INTERNAL_SERVER_ERROR, 'Something went wrong ¯\_(ツ)_/¯', response));
                    }
                })
                .catch((error) => {
                    res.status(httpCodes.INTERNAL_SERVER_ERROR);
                    res.json(formatResponse(false, 3, 'User is already checked in', error));
                });
            })
            .catch((error) => {
                if (!error) {
                    // stuff has been handled already.
                } else {
                    dbDefaultError(res, error);
                }
            });

        } else {
            paramNotSpecified('cardId', 'readerId');
        }
    } else {
        // not specified, so throw an error. This call requires option
        res.status(httpCodes.BAD_REQUEST);
        res.json(formatResponse(false, httpCodes.BAD_REQUEST, 'The option parameter (in/out) is not specified correctly.'));
    }
});

router.get('/users/anonymous/', (req, res) => {
    const query = db.format('SELECT * FROM user WHERE NAME = ?', [ANONYMOUS]);
    db.execute(query)
    .then((response) => {
        console.log(response.results);
        res.status(httpCodes.OK)
        res.json(formatResponse(true, httpCodes.OK, 'Here are your anonymous users.', response.results));
    })
    .catch((error) => {
        console.log(error);
        dbDefaultError(res, error);
    });
});

router.get('/users/:userId?', (req, res) => {
    const userId = req.params.userId || null;
    var query = db.format('SELECT * FROM user;');
    if (userId) {
        // id specified.
        query = db.format('SELECT * FROM user WHERE ID = ?', [userId]);
    }
    defaultGetQuery(res, query, 'user');
});

router.put('/users/:id?', (req, res) => {
    const body = req.body || null;
    const userId = req.params.id || null;
    const {name, cardId, role} = body;

    if (name && cardId && role && userId) {
        const query = db.format('UPDATE user SET NAME = ?, CARD_ID = ?, ROLE = ? WHERE ID = ?', [name, cardId, role, userId]);
        db.execute(query)
        .then((response) => {
            console.log(response);
            res.status(httpCodes.OK);
            res.json(formatResponse(true, httpCodes.OK, 'User succesfully updated.', response.results));
        })
        .catch((error) => {
            console.log(error);
            dbDefaultError(res, error);
        });
    } else {
        res.status(httpCodes.BAD_REQUEST);
        res.json(formatResponse(false, httpCodes.BAD_REQUEST, 'Could not update user because the required information (name, cardId, role, userId) is missing '));
    }

});

router.post('/users/', (req, res) => {
    const body = req.body || null;
    const {name, cardId, role} = body;

    if (name && cardId && role) {

        const query = db.format('INSERT INTO user (NAME, CARD_ID, ROLE) VALUES (?, ?, ?)', [name, cardId, role]);
        db.execute(query)
        .then((response) => {
            console.log(response);
            if (response.results) {
                res.status(httpCodes.OK);
                res.json(formatResponse(true, httpCodes.OK, 'User succesfully created.', response.results));
            }
        })
        .catch((error) => {
            if (error.errno === 1062) {
                res.status(httpCodes.INTERNAL_SERVER_ERROR);
                res.json(formatResponse(false, httpCodes.INTERNAL_SERVER_ERROR, 'This user already exists in the database.', body));
                return;
            }
            dbDefaultError(res, error);
        });
    } else {
        res.status(httpCodes.BAD_REQUEST);
        res.json(formatResponse(false, httpCodes.BAD_REQUEST, 'Could not add user because the required information (name, cardId, role) is missing '));
    }

});

export default router;
